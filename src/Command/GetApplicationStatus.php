<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetApplicationStatus
 * @package RBS\Selifa\Composer\Command
 */
class GetApplicationStatus extends SelifaBaseCommand
{
    protected function configure()
    {
        $this->setName('selifa-app-status');
        $this->setDescription('Get brief information about applications in this installation.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $appCount = $this->Core->ComposeData->GetApplicationCount();
        $output->writeln('No. of application: <fg=green>'.$appCount.'</>',true);

        $appKeys = $this->Core->ComposeData->GetApplicationKeys();
        foreach ($appKeys as $key)
        {
            $spec = $this->Core->ComposeData->GetApplication($key);
            if ($spec !== null)
            {
                $output->write('  - <fg=green>'.$spec['key'].'</>, '.$spec['name'],true);
            }
        }
    }
}
?>