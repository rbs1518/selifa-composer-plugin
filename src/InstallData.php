<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;

/**
 * Class InstallData
 * @package RBS\Selifa\Composer
 */
class InstallData extends BaseDataObject
{
    /**
     *
     */
    protected function InitializeIfFileNotExists()
    {
        $this->Data = array(
            'packages' => array(),
            'ignores' => array(
                '/vendor/',
                '/temps/'
            )
        );

        $corePath = ($this->WorkingDir.'selifa');
        $coreFiles = array();
        $this->Core->FM->EnumerateDirectory($corePath,'',$coreFiles);
        $this->AddPackageInfo('core','','','');

        foreach ($coreFiles as $item)
        {
            $fileItem = ('selifa/'.$item);
            $this->AddPackageFile('core',$fileItem);
        }
    }

    /**
     * @param string $pName
     * @param string $libPath
     * @param string $cfgPath
     * @param string $drvPath
     * @param string $scorePath
     */
    public function AddPackageInfo($pName,$libPath,$cfgPath,$drvPath,$scorePath='selifa')
    {
        if (isset($this->Data['packages'][$pName]))
            unset($this->Data['packages'][$pName]);
        $this->Data['packages'][$pName] = array(
            'source-paths' => array(
                'lib' => $libPath,
                'cfg' => $cfgPath,
                'drv' => $drvPath,
                'cre' => $scorePath
            ),
            'files' => array(),
            'configurations' => array()
        );
    }

    /**
     * @param string $pName
     * @param string $libPath
     * @param string $cfgPath
     * @param string $drvPath
     * @param string $scorePath
     */
    public function ResetPackageInfo($pName,$libPath,$cfgPath,$drvPath,$scorePath='selifa')
    {
        if (!isset($this->Data['packages'][$pName]))
            return;
        $this->Data['packages'][$pName]['source-paths']['lib'] = $libPath;
        $this->Data['packages'][$pName]['source-paths']['cfg'] = $cfgPath;
        $this->Data['packages'][$pName]['source-paths']['drv'] = $drvPath;
        $this->Data['packages'][$pName]['source-paths']['cre'] = $scorePath;
        $this->Data['packages'][$pName]['files'] = array();
        $this->Data['packages'][$pName]['configurations'] = array();
    }

    /**
     * @param string $pName
     * @param string $file
     */
    public function AddPackageFile($pName,$file)
    {
        if (isset($this->Data['packages'][$pName]['files']))
            $this->Data['packages'][$pName]['files'][] = $file;
    }

    /**
     * @param string $pName
     * @param string $key
     * @param array $data
     */
    public function AddConfigurationItem($pName,$key,$data)
    {
        if (isset($this->Data['packages'][$pName]['configurations']))
            $this->Data['packages'][$pName]['configurations'][$key] = $data;
    }

    /**
     * @param $pName
     */
    public function RemovePackageInfo($pName)
    {
        if (isset($this->Data['packages'][$pName]))
            unset($this->Data['packages'][$pName]);
    }

    /**
     * @param string $pName
     * @return bool
     */
    public function IsPackageExists($pName)
    {
        return isset($this->Data['packages'][$pName]);
    }

    /**
     * @return array
     */
    public function GetAllConfigurations()
    {
        $config = array();
        foreach ($this->Data['packages'] as $pName => $meta)
        {
            $pConfig = $meta['configurations'];
            if (count($pConfig) > 0)
            {
                foreach ($pConfig as $key => $options)
                    $config[$key] = $options;
            }
        }
        return $config;
    }
}
?>