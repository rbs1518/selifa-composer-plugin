<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;
use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Exception;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Core
 * @package RBS\Selifa\Composer
 */
class Core
{
    /**
     * @var null|Core
     */
    private static $_Instance = null;

    /**
     * @var null|Composer
     */
    public $Composer = null;

    /**
     * @var null|ComposeData
     */
    public $ComposeData = null;

    /**
     * @var null|InstallData
     */
    public $InstallData = null;

    /**
     * @var null|IOInterface
     */
    public $IO = null;

    /**
     * @var null|FileManager
     */
    public $FM = null;

    /**
     * @var null|FileGenerator
     */
    public $Generator = null;

    /**
     * @var string
     */
    public $RootDir = '';

    /**
     * @var string
     */
    public $TempPath = '';

    /**
     * @var string
     */
    public $LibraryPath = '';

    /**
     * @var string
     */
    public $ConfigPath = '';

    /**
     * @var string
     */
    public $DriverPath = '';

    /**
     * @var string
     */
    public $CorePath = '';

    /**
     * @param Composer $composer
     * @param IOInterface $io
     * @return null|Core
     */
    public static function Initialize(Composer $composer, IOInterface $io)
    {
        if (self::$_Instance == null)
            self::$_Instance = new Core($composer,$io);
        return self::$_Instance;
    }

    /**
     * @return null|Core
     * @throws Exception
     */
    public static function GetObject()
    {
        if (self::$_Instance == null)
            throw new Exception('Uninitialized core object.');
        return self::$_Instance;
    }

    private function __construct(Composer $composer, IOInterface $io)
    {
        $this->Composer = $composer;
        $this->IO = $io;
        $this->RootDir = (getcwd().DIRECTORY_SEPARATOR);

        $this->Generator = new FileGenerator();
        $this->FM = new FileManager($io);

        $this->ComposeData = new ComposeData($this,'.selifa-compose.json');
        $this->InstallData = new InstallData($this,'.selifa-install.json');

        $this->_InitializePath($composer,'TempPath','selifa-temp-path','temps',0777);
        $this->_InitializePath($composer,'LibraryPath','selifa-library-path','libraries');
        $this->_InitializePath($composer,'DriverPath','selifa-driver-path','drivers');
        $this->_InitializePath($composer,'CorePath','selifa-core-path','selifa');
    }

    /**
     * @param Composer $c
     * @param string $propName
     * @param string $cfgKey
     * @param string $default
     * @param int $permission
     */
    private function _InitializePath(Composer $c, $propName,$cfgKey,$default,$permission=0775)
    {
        $path = $c->getConfig()->get($cfgKey);
        if ($path === null)
            $fixPath = ($default.DIRECTORY_SEPARATOR);
        else
            $fixPath = ($path.DIRECTORY_SEPARATOR);
        if (!file_exists($this->RootDir.$fixPath))
            mkdir($this->RootDir.$fixPath,$permission,true);
        $this->{$propName} = $fixPath;
    }

    /**
     *
     */
    public function Finalize()
    {
        $this->ComposeData->WriteToFile();
        $this->InstallData->WriteToFile();
    }

    /**
     *
     */
    public function CreateGitIgnoreFile()
    {
        $gitIgnoreFile = ($this->RootDir.'.gitignore');
        if ($this->ComposeData->IsCreateGitIgnore())
        {
            $this->IO->write('<fg=cyan>Generating .gitignore file</>');
            $this->Generator->GenerateGitIgnoreFiles($gitIgnoreFile,$this->InstallData);
        }
    }

    /**
     * @param PackageInterface $package
     * @return string
     */
    public function GetTemporaryPath(PackageInterface $package)
    {
        $baseTempPath = ($this->RootDir.$this->TempPath);
        $path = ($baseTempPath.'composer-installs'.DIRECTORY_SEPARATOR.$package->getPrettyName().DIRECTORY_SEPARATOR);
        if (!file_exists($path))
            mkdir($path,0777,true);
        return $path;
    }

    #region Inner Installation Functions
    /**
     * @param string $libDir
     * @param string $pName
     * @param string $vtPrefix
     */
    protected function InstallLibraries($libDir,$pName,$vtPrefix='  - ')
    {
        if (file_exists($libDir))
        {
            $this->IO->write($vtPrefix.'Installing <fg=green>'.$pName.'</> libraries ',true,4);
            $targetPath = ($this->RootDir.$this->LibraryPath);
            $libFiles = array();
            $this->FM->EnumerateDirectory($libDir,'',$libFiles);
            foreach ($libFiles as $fileName)
            {
                $this->IO->write('  - <fg=cyan>Installing</> '.$fileName.' ',false,4);
                try
                {
                    $dir = dirname($targetPath.$fileName);
                    if (!file_exists($dir))
                        mkdir($dir,0775,true);
                    copy($libDir.DIRECTORY_SEPARATOR.$fileName,$targetPath.$fileName);
                    $this->InstallData->AddPackageFile($pName,$this->LibraryPath.$fileName);
                    $this->IO->write("<fg=green>[ OK ]</>",false,4);
                }
                catch (Exception $x)
                {
                    $msgs = explode(':',$x->getMessage(),2);
                    $this->IO->write("<fg=red>[ ".trim($msgs[1])." ]</>",false,4);
                }
                $this->IO->write("",true,4);
            }
            $this->IO->write($vtPrefix.'<fg=green>'.$pName.'</> installed.',true,4);
        }
    }

    /**
     * @param string $drvDir
     * @param string $pName
     * @param string $vtPrefix
     */
    protected function InstallDrivers($drvDir,$pName,$vtPrefix='  - ')
    {
        if (file_exists($drvDir))
        {
            $this->IO->write($vtPrefix.'Installing <fg=green>'.$pName.'</> driver(s) ',true,4);
            $targetPath = ($this->RootDir.$this->DriverPath);
            $driverFiles = array();
            $this->FM->EnumerateDirectory($drvDir,'',$driverFiles);
            foreach ($driverFiles as $fileName)
            {
                $this->IO->write('  - <fg=cyan>Installing</> '.$fileName.' ',false,4);
                try
                {
                    $dir = dirname($targetPath.$fileName);
                    if (!file_exists($dir))
                        mkdir($dir,0775,true);
                    copy($drvDir.DIRECTORY_SEPARATOR.$fileName,$targetPath.$fileName);
                    $this->InstallData->AddPackageFile($pName,$this->DriverPath.$fileName);
                }
                catch (Exception $x)
                {
                    $msgs = explode(':',$x->getMessage(),2);
                    $this->IO->write("<fg=red>[ ".trim($msgs[1])." ]</>",false,4);
                }
                $this->IO->write("",true,4);
            }
            $this->IO->write($vtPrefix.'<fg=green>'.$pName.'</> driver(s) installed.',true,4);
        }
    }

    /**
     * @param string $scoreDir
     * @param string $pName
     * @param string $vtPrefix
     */
    protected function InstalltoSelifaCore($scoreDir,$pName,$vtPrefix='')
    {
        if (file_exists($scoreDir))
        {
            $this->IO->write($vtPrefix.'Installing <fg=green>'.$pName.'</> into selifa core ',true,4);
            $targetPath = ($this->RootDir.$this->CorePath);
            $coreFiles = array();
            $this->FM->EnumerateDirectory($scoreDir,'',$coreFiles);
            foreach ($coreFiles as $fileName)
            {
                $this->IO->write('  - <fg=cyan>Installing</> '.$fileName.' ',false,4);
                try
                {
                    $dir = dirname($targetPath.$fileName);
                    if (!file_exists($dir))
                        mkdir($dir,0775,true);
                    copy($scoreDir.DIRECTORY_SEPARATOR.$fileName,$targetPath.$fileName);
                    $this->InstallData->AddPackageFile($pName,$this->CorePath.$fileName);
                    $this->IO->write("<fg=green>[ OK ]</>",false,4);
                }
                catch (Exception $x)
                {
                    $msgs = explode(':',$x->getMessage(),2);
                    $this->IO->write("<fg=red>[ ".trim($msgs[1])." ]</>",false,4);
                }
                $this->IO->write("",true,4);
            }
            $this->IO->write($vtPrefix.'<fg=green>'.$pName.'</> installed.',true,4);
        }
    }

    /**
     * @param string $cfgDir
     * @param string $pName
     * @param string $vtPrefix
     * @return int
     */
    protected function InstallConfigurations($cfgDir,$pName,$vtPrefix='  - ')
    {
        $iCount = 0;
        if (file_exists($cfgDir))
        {
            $configFiles = array();
            $this->FM->EnumerateDirectory($cfgDir,'',$configFiles);
            foreach ($configFiles as $fileName)
            {
                $config_file = ($cfgDir.DIRECTORY_SEPARATOR.$fileName);
                $base_name = basename($config_file,'.php');
                $this->IO->write($vtPrefix."Installing configuration <fg=green>".$base_name.'</>.');
                $options = include($config_file);
                $this->InstallData->AddConfigurationItem($pName,$base_name,$options);
                $iCount++;
            }
        }
        return $iCount;
    }

    /**
     * @param string $pName
     * @param string $vtPrefix
     * @throws Exception
     */
    protected function RemoveInstalledFiles($pName,$vtPrefix='  - ')
    {
        $packages = $this->InstallData->GetData()['packages'];
        if (!$this->InstallData->IsPackageExists($pName))
            throw new Exception('Package '.$pName.' is not installed.');

        $ps = $packages[$pName];
        $excludes = array($this->TempPath,$this->LibraryPath,$this->DriverPath);
        $dirUniques = $this->FM->GetUniqueDirectoryNameForAllLevels($ps['files'],$excludes);
        foreach ($ps['files'] as $item)
        {
            $fn = ($this->RootDir.$item);
            $this->IO->write($vtPrefix."<fg=red>Remove</> ".$item." ",false,4);
            try
            {
                unlink($fn);
                $this->IO->write("<fg=green>[ OK ]</>",false,4);
            }
            catch (Exception $x)
            {
                $msgs = explode(':',$x->getMessage(),2);
                $this->IO->write("<fg=red>[ ".trim($msgs[1])." ]</>",false,4);
            }
            $this->IO->write("",true,4);
        }

        foreach ($dirUniques as $item)
        {
            $fn = ($this->RootDir.$item);
            $this->IO->write($vtPrefix."<fg=red>Remove</> ".$item." ",false,4);
            try
            {
                if (file_exists($fn))
                {
                    if ($this->FM->IsDirectoryEmpty($fn))
                    {
                        rmdir($fn);
                        $this->IO->write("<fg=green>[ OK ]</>",false,4);
                    }
                    else
                        $this->IO->write("<fg=cyan>[ Skipped, directory is not empty ]</>",false,4);
                }
                else
                    $this->IO->write("<fg=red>[ directory is not exists ]</>",false,4);
            }
            catch (Exception $x)
            {
                $msgs = explode(':',$x->getMessage(),2);
                $this->IO->write("<fg=red>[ ".trim($msgs[1])." ]</>",false,4);
            }
            $this->IO->write("",true,4);
        }
    }
    #endregion

    /**
     * @param PackageInterface $package
     * @param string $path
     */
    public function ProcessInstallation(PackageInterface $package,$path)
    {
        $extra = $package->getExtra();
        if (isset($extra['source-path']))
            $sourceDir = ($path.trim($extra['source-path']).'/');
        else
            $sourceDir = ($path.'src/');

        if (isset($extra['libraries-path']))
            $libDir = ($sourceDir.trim($extra['libraries-path']));
        else
            $libDir = ($sourceDir.'libraries');

        if (isset($extra['config-path']))
            $cfgDir = ($sourceDir.trim($extra['config-path']));
        else
            $cfgDir = ($sourceDir.'configs');

        if (isset($extra['driver-path']))
            $drvDir = ($sourceDir.trim($extra['driver-path']));
        else
            $drvDir = ($sourceDir.'drivers');

        if (isset($extra['selifa-core-path']))
            $scoreDir = ($sourceDir.trim($extra['selifa-core-path']));
        else
            $scoreDir = ($sourceDir.'selifa');

        $pName = $package->getPrettyName();
        $this->InstallData->AddPackageInfo($pName,$libDir,$cfgDir,$drvDir,$scoreDir);

        $this->InstallLibraries($libDir,$pName);
        $this->InstallDrivers($drvDir,$pName);
        $this->InstalltoSelifaCore($scoreDir,$pName);

        $iCount = $this->InstallConfigurations($cfgDir,$pName);
        Session::IncrementCount('ConfigurationInstall',$iCount);
    }

    /**
     * @param PackageInterface $initial
     * @param PackageInterface $target
     * @param string $path
     * @throws Exception
     */
    public function ProcessUpdate(PackageInterface $initial,PackageInterface $target,$path)
    {
        $extra = $target->getExtra();
        if (isset($extra['source-path']))
            $sourceDir = ($path.trim($extra['source-path']).'/');
        else
            $sourceDir = ($path.'src/');

        if (isset($extra['libraries-path']))
            $libDir = ($sourceDir.trim($extra['libraries-path']));
        else
            $libDir = ($sourceDir.'libraries');

        if (isset($extra['config-path']))
            $cfgDir = ($sourceDir.trim($extra['config-path']));
        else
            $cfgDir = ($sourceDir.'configs');

        if (isset($extra['"driver-path']))
            $drvDir = ($sourceDir.trim($extra['"driver-path']));
        else
            $drvDir = ($sourceDir.'drivers');

        if (isset($extra['selifa-core-path']))
            $scoreDir = ($sourceDir.trim($extra['selifa-core-path']));
        else
            $scoreDir = ($sourceDir.'selifa');

        $pName = $target->getPrettyName();
        $this->IO->write('Updating package <fg=green>'.$pName.'</> ',false);
        $this->RemoveInstalledFiles($pName);

        $this->IO->write('  - Reset package information ',false,4);
        $this->InstallData->ResetPackageInfo($pName,$libDir,$cfgDir,$drvDir,$scoreDir);
        $this->IO->write("<fg=green>[ OK ]</>",true,4);

        $this->InstallLibraries($libDir,$pName);
        $this->InstallDrivers($drvDir,$pName);
        $this->InstalltoSelifaCore($scoreDir,$pName);

        $iCount = $this->InstallConfigurations($cfgDir,$pName);
        Session::IncrementCount('ConfigurationInstall',$iCount);
    }

    /**
     * @param PackageInterface $package
     * @throws Exception
     */
    public function ProcessRemoval(PackageInterface $package)
    {
        $pName = $package->getPrettyName();
        $this->IO->write('  - Removing package <fg=red>'.$pName.'</> ...',true);

        $this->RemoveInstalledFiles($pName);
        $this->InstallData->RemovePackageInfo($pName);
        $this->IO->write('  - Package <fg=red>'.$pName.'</> removed.',true);
    }
}
?>