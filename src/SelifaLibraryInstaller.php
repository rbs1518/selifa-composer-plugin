<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;
use Composer\Composer;
use Composer\Installer\InstallerInterface;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use Composer\Downloader\DownloadManager;
use Exception;
use InvalidArgumentException;

/**
 * Class SelifaLibraryInstaller
 * @package RBS\Selifa\Composer
 */
class SelifaLibraryInstaller implements InstallerInterface
{
    /**
     * @var Composer
     */
    protected $Composer;

    /**
     * @var DownloadManager
     */
    protected $DM;

    /**
     * @var null|Core
     */
    protected $Core = null;

    /**
     * SelifaLibraryInstaller constructor.
     * @param Core $core
     */
    public function __construct(Core $core)
    {
        $this->Composer = $core->Composer;
        $this->DM = $core->Composer->getDownloadManager();
        $this->Core = $core;
    }

    #region InstallerInterface Implementation
    /**
     * @param string $packageType
     * @return bool
     */
    public function supports($packageType)
    {
        return ($packageType == 'selifa-library');
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     * @return bool
     */
    public function isInstalled(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        $pName = $package->getPrettyName();
        return ($repo->hasPackage($package) && $this->Core->InstallData->IsPackageExists($pName));
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     */
    public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        $downloadPath = $this->Core->GetTemporaryPath($package);
        $this->DM->download($package,$downloadPath);
        $this->Core->ProcessInstallation($package,$downloadPath);
        if (!$repo->hasPackage($package))
            $repo->addPackage(clone $package);
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $initial
     * @param PackageInterface $target
     */
    public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
    {
        if (!$repo->hasPackage($initial))
            throw new InvalidArgumentException('Package is not installed: '.$initial);
        $downloadPath = $this->Core->GetTemporaryPath($target);
        $this->DM->download($target,$downloadPath);
        $this->Core->ProcessUpdate($initial,$target,$downloadPath);
        $repo->removePackage($initial);
        if (!$repo->hasPackage($target))
            $repo->addPackage(clone $target);
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     * @throws Exception
     */
    public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        if (!$repo->hasPackage($package))
            throw new InvalidArgumentException('Package is not installed: '.$package);
        try
        {
            $downloadPath = $this->Core->GetTemporaryPath($package);

            $fm = new FileManager($this->Core->IO);
            $fm->DeleteDirectory(rtrim($downloadPath,DIRECTORY_SEPARATOR));
            rmdir($downloadPath);
        }
        catch (Exception $x) { }
        $this->Core->ProcessRemoval($package);
        $repo->removePackage($package);
    }

    /**
     * @param PackageInterface $package
     * @return string
     */
    public function getInstallPath(PackageInterface $package)
    {
        return $this->Core->GetTemporaryPath($package);
    }
    #endregion
}
?>