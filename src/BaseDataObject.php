<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;
use Composer\Json\JsonFile;

/**
 * Class BaseDataObject
 * @package RBS\Selifa\Composer
 */
abstract class BaseDataObject
{
    /**
     * @var null|JsonFile
     */
    private $_JSON = null;

    /**
     * @var string
     */
    protected $WorkingDir = '';

    /**
     * @var null|array
     */
    protected $Data = null;

    /**
     * @var null|Core
     */
    protected $Core = null;

    /**
     * BaseDataObject constructor.
     * @param Core $core
     * @param string $dataFile
     */
    public function __construct(Core $core,$dataFile)
    {
        $this->Core = $core;
        $this->WorkingDir = (getcwd().'/');
        $this->_JSON = new JsonFile($this->WorkingDir.$dataFile,null,$core->IO);
        if ($this->_JSON->exists())
            $this->Data = $this->_JSON->read();
        else
            $this->InitializeIfFileNotExists();
    }

    /**
     *
     */
    public function WriteToFile()
    {
        $this->_JSON->write($this->Data);
    }

    /**
     * @return array|null
     */
    public function GetData()
    {
        return $this->Data;
    }

    abstract protected function InitializeIfFileNotExists();
}
?>