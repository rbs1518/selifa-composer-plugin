<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;
use Composer\IO\IOInterface;

/**
 * Class FileManager
 * @package RBS\Selifa\Composer
 */
class FileManager
{
    /**
     * @var null|IOInterface
     */
    protected $IO = null;

    /**
     * @param IOInterface $io
     */
    public function __construct(IOInterface $io)
    {
        $this->IO = $io;
    }

    /**
     * @param string $path
     * @return bool|null
     */
    public function IsDirectoryEmpty($path)
    {
        if (!is_readable($path))
            return null;
        $handle = opendir($path);
        while (false !== ($entry = readdir($handle)))
        {
            if (($entry != ".") && ($entry != ".."))
                return false;
        }
        return true;
    }

    /**
     * @param string $path
     * @param array $array
     * @param array $excludes
     */
    public function ParseDirectoryLevels($path,&$array,$excludes=array())
    {
        $nextDN = dirname($path);
        if (($nextDN == '.') || ($nextDN == ''))
            return;
        if (in_array($nextDN.DIRECTORY_SEPARATOR,$excludes))
            return;
        else
        {
            if (!in_array($nextDN,$array))
            {
                $array[] = $nextDN;
                $this->ParseDirectoryLevels($nextDN,$array,$excludes);
            }
            return;
        }
    }

    /**
     * @param array $source
     * @param array $excludes
     * @return array
     */
    public function GetUniqueDirectoryNameForAllLevels($source,$excludes=array())
    {
        $result = array();
        foreach ($source as $item)
        {
            $dn = dirname($item);
            if (!in_array($dn, $result))
            {
                $result[] = $dn;
                $this->ParseDirectoryLevels($dn,$result,$excludes);
            }
        }
        rsort($result,SORT_STRING);
        return $result;
    }

    /**
     * @param string $baseDir
     * @param string $dir
     * @param array $array
     */
    public function EnumerateDirectory($baseDir,$dir,&$array)
    {
        if ($dir == '')
            $cdir = scandir($baseDir);
        else
            $cdir = scandir($baseDir.DIRECTORY_SEPARATOR.$dir);
        foreach ($cdir as $key => $value)
        {
            if (!in_array($value,array(".","..")))
            {
                $fullPath = ($baseDir.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$value);
                if (is_dir($fullPath))
                {
                    if ($dir == '')
                        $nDir = ($value);
                    else
                        $nDir = ($dir.DIRECTORY_SEPARATOR.$value);
                    $this->EnumerateDirectory($baseDir,$nDir,$array);
                }
                else
                {
                    if ($dir == '')
                        $array[] = $value;
                    else
                        $array[] = ($dir.DIRECTORY_SEPARATOR.$value);
                }
            }
        }
    }

    /**
     * @param string $dir
     * @return bool
     */
    public function DeleteDirectory($dir)
    {
        if (is_dir($dir))
            $dir_handle = opendir($dir);
        if (!$dir_handle)
            return false;
        while($file = readdir($dir_handle))
        {
            if ($file != "." && $file != "..")
            {
                if (!is_dir($dir.DIRECTORY_SEPARATOR.$file))
                    unlink($dir.DIRECTORY_SEPARATOR.$file);
                else
                    $this->DeleteDirectory($dir.DIRECTORY_SEPARATOR.$file);
            }
        }
        closedir($dir_handle);
        rmdir($dir);
        return true;
    }
}
?>