<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;

/**
 * Class ComposeData
 * @package RBS\Selifa\Composer
 */
class ComposeData extends BaseDataObject
{
    /**
     *
     */
    protected function InitializeIfFileNotExists()
    {
        $this->Data = array(
            'create-gitignore' => false,
            'app-dir' => 'apps',
            'temp-dir' => 'temps',
            'lib-dir' => 'libraries',
            'default-paths' => array(
                'config' => 'configs'
            ),
            'default-config' => array(),
            'applications' => array()
        );
    }

    /**
     * @param string $id
     */
    public function SetupDefaultConfiguration($id)
    {
        if (!isset($this->Data['default-config'][$id]))
            $this->Data['default-config'][$id] = array();
    }

    /**
     * @return bool
     */
    public function IsCreateGitIgnore()
    {
        if (isset($this->Data['create-gitignore']))
            return (bool)$this->Data['create-gitignore'];
        else
            return false;
    }

    /**
     * @return string
     */
    public function GetApplicationPath()
    {
        if (isset($this->Data['app-dir']))
            return $this->Data['app-dir'];
        else
            return 'apps';
    }

    /**
     * @return array
     */
    public function GetDefaultPaths()
    {
        if (isset($this->Data['default-paths']))
            return $this->Data['default-paths'];
        else
            return array();
    }

    /**
     * @return bool
     */
    public function IsApplicationExists()
    {
        $apps = $this->Data['applications'];
        return (count($apps) > 0);
    }

    /**
     * @param string $key
     * @return null|array
     */
    public function GetApplication($key)
    {
        if (isset($this->Data['applications'][$key]))
            return $this->Data['applications'][$key];
        else
            return null;
    }

    /**
     * @param string $key
     * @param array $spec
     */
    public function SetApplication($key,$spec)
    {
        if (isset($this->Data['applications'][$key]))
            $this->Data['applications'][$key] = array_replace_recursive($this->Data['applications'][$key],$spec);
        else
            $this->Data['applications'][$key] = $spec;
    }

    /**
     * @return int
     */
    public function GetApplicationCount()
    {
        return (count($this->Data['applications']));
    }

    /**
     * @return array
     */
    public function GetApplicationKeys()
    {
        $keys = array();
        foreach ($this->Data['applications'] as $key => $spec)
            $keys[] = $key;
        return $keys;
    }
}
?>