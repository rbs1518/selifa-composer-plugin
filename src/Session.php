<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;

/**
 * Class Session
 * @package RBS\Selifa\Composer
 */
class Session
{
    /**
     * @var null|Session
     */
    private static $_Instance = null;

    /**
     * @var int
     */
    private $_ConfigurationInstallCount = 0;

    /**
     * @return null|Session
     */
    public static function Initialize()
    {
        if (self::$_Instance === null)
            self::$_Instance = new Session();
        return self::$_Instance;
    }

    /**
     *
     */
    public static function ResetCounts()
    {
        if (self::$_Instance != null)
        {
            self::$_Instance->_ConfigurationInstallCount = 0;
        }
    }

    /**
     * @param string $key
     */
    public static function ResetCount($key)
    {
        if (self::$_Instance != null)
        {
            $propName = ('_'.$key.'Count');
            if (property_exists(self::$_Instance,$propName))
            {
                if (self::$_Instance->{$propName} == null)
                    self::$_Instance->{$propName} = 0;
                self::$_Instance->{$propName} = 0;
            }
        }
    }

    /**
     * @param string $key
     * @param int $by
     */
    public static function IncrementCount($key,$by=1)
    {
        if (self::$_Instance != null)
        {
            $propName = ('_'.$key.'Count');
            if (property_exists(self::$_Instance,$propName))
            {
                if (self::$_Instance->{$propName} == null)
                    self::$_Instance->{$propName} = 0;
                self::$_Instance->{$propName} += $by;
            }
        }
    }

    /**
     * @param string $key
     * @param int $by
     */
    public static function DecrementCount($key,$by=1)
    {
        if (self::$_Instance != null)
        {
            $propName = ('_'.$key.'Count');
            if (property_exists(self::$_Instance,$propName))
            {
                if (self::$_Instance->{$propName} == null)
                    self::$_Instance->{$propName} = 0;
                self::$_Instance->{$propName} -= $by;
            }
        }
    }

    /**
     * @param string $key
     * @return int
     */
    public static function GetCount($key)
    {
        if (self::$_Instance != null)
        {
            $propName = ('_'.$key.'Count');
            if (property_exists(self::$_Instance,$propName))
            {
                if (self::$_Instance->{$propName} == null)
                    self::$_Instance->{$propName} = 0;
                return (int)self::$_Instance->{$propName};
            }
        }
        return 0;
    }
}
?>