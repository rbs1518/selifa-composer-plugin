<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;

use Composer\IO\IOInterface;

/**
 * Class Application
 * @package RBS\Selifa\Composer
 */
class Application
{
    /**
     * @var null|IOInterface
     */
    protected $IO = null;

    /**
     * @var null|ComposeData
     */
    protected $ComposeData = null;

    /**
     * @var null|Core
     */
    protected $Core = null;

    /**
     * @var null|FileGenerator
     */
    protected $FG = null;

    /**
     * Application constructor.
     * @param Core $core
     * @param FileGenerator $fileGen
     */
    public function __construct(Core $core, FileGenerator $fileGen)
    {
        $this->Core = $core;
        $this->IO = $core->IO;
        $this->ComposeData = $core->ComposeData;
        $this->FG = $fileGen;
    }

    /**
     * @param string $key
     * @param string $name
     * @return bool
     */
    public function Create($key,$name='')
    {
        $appPath = ($this->ComposeData->GetApplicationPath().DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR);
        if (!file_exists($this->Core->RootDir.$appPath))
            mkdir($this->Core->RootDir.$appPath,0775,true);

        $defPaths = $this->ComposeData->GetDefaultPaths();

        $indexFile = ($this->Core->RootDir.$appPath.'index.php');
        $app = $this->ComposeData->GetApplication($key);
        if (($app !== null) || file_exists($indexFile))
        {
            $this->IO->write('[<fg=red>WARNING</>] Application <fg=cyan>'.$key.'</> already exists. Please use different key.',true);
            return false;
        }

        if (isset($defPaths['config']))
            $configPath = ($appPath.$defPaths['config'].DIRECTORY_SEPARATOR);
        else
            $configPath = ($appPath.'configs'.DIRECTORY_SEPARATOR);

        $spec = array(
            'key' => $key,
            'name' => $name,
            'paths' => array(
                'app' => $appPath
            ),
            'config' => array()
        );

        foreach ($defPaths as $pathKey => $path)
        {
            $_path = ($appPath.$path.DIRECTORY_SEPARATOR);
            if (!file_exists($_path))
                mkdir($_path,0775,true);
            $spec['paths'][$pathKey] = $_path;
        }

        $this->FG->GenerateIndexFile($indexFile,$configPath);
        $this->ComposeData->SetApplication($key,$spec);
        return true;
    }

    public function Update($key)
    {
        $app = $this->ComposeData->GetApplication($key);
        $appPath = ($this->ComposeData->GetApplicationPath().DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR);
        if (($app == null) || !file_exists($appPath))
        {
            $this->IO->write('[<fg=red>WARNING</>] Application <fg=cyan>'.$key.'</> does not exists.',true);
            return false;
        }

        $this->IO->write('Updating <fg=green>'.$app['name'].'</> ...',true);

        $defPaths = $this->ComposeData->GetDefaultPaths();
        foreach ($defPaths as $pathKey => $path)
        {
            if (!isset($app['paths'][$pathKey]))
            {
                $_path = ($appPath.$path.DIRECTORY_SEPARATOR);
                if (!file_exists($_path))
                    mkdir($_path,0775,true);
                $app['paths'][$pathKey] = $_path;
                $this->IO->write('  - Creating path <fg=cyan>'.$_path.'</>.',true);
            }
        }

        if (isset($app['paths']['config']))
            $configPath = ($this->Core->RootDir.$app['paths']['config']);
        else if (isset($defPaths['config']))
            $configPath = ($this->Core->RootDir.$defPaths['config'].DIRECTORY_SEPARATOR);
        else
            $configPath = ($this->Core->RootDir.'configs'.DIRECTORY_SEPARATOR);

        $iConfigs = $this->Core->InstallData->GetAllConfigurations();
        foreach ($iConfigs as $cKey => $options)
        {
            $aOptions = $options;
            if (isset($app['config'][$cKey]))
                $aOptions = array_replace_recursive($aOptions,$app['config'][$cKey]);
            $file = ($configPath.$cKey.'.php');
            $this->FG->GenerateConfigurationFile($file,$aOptions);
            $this->IO->write('  - Creating configuration <fg=cyan>'.$cKey.'</>.',true);
        }

        $this->IO->write('<fg=green>'.$app['name'].'</> updated',true);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function UpdateConfigurationOnly($key)
    {
        $app = $this->ComposeData->GetApplication($key);
        $appPath = ($this->ComposeData->GetApplicationPath().DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR);
        if (($app == null) || !file_exists($appPath))
        {
            $this->IO->write('[<fg=red>WARNING</>] Application <fg=cyan>'.$key.'</> does not exists.',true);
            return false;
        }

        $this->IO->write('Updating <fg=green>'.$app['name'].'</> configuration ...',true);

        if (isset($app['paths']['config']))
            $configPath = ($this->Core->RootDir.$app['paths']['config']);
        else if (isset($defPaths['config']))
            $configPath = ($this->Core->RootDir.$defPaths['config'].DIRECTORY_SEPARATOR);
        else
            $configPath = ($this->Core->RootDir.'configs'.DIRECTORY_SEPARATOR);

        $iConfigs = $this->Core->InstallData->GetAllConfigurations();
        foreach ($iConfigs as $cKey => $options)
        {
            $aOptions = $options;
            if (isset($app['config'][$cKey]))
                $aOptions = array_replace_recursive($aOptions,$app['config'][$cKey]);
            $file = ($configPath.$cKey.'.php');
            $this->FG->GenerateConfigurationFile($file,$aOptions);
            $this->IO->write('  - Creating configuration <fg=cyan>'.$cKey.'</>.',true);
        }

        $this->IO->write('<fg=green>'.$app['name'].'</> configuration updated.',true);
    }
}
?>