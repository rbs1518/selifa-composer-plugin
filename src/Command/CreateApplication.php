<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Command;
use RBS\Selifa\Composer\Application;
use RBS\Selifa\Composer\FileGenerator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateApplication
 * @package RBS\Selifa\Composer\Command
 */
class CreateApplication extends SelifaBaseCommand
{
    protected function configure()
    {
        $this->setName('selifa-create-app');

        $this->addOption('key','k',InputOption::VALUE_OPTIONAL,"Application's identifier. Default to 'default' if not specified.",'default');
        $this->addOption('name','a',InputOption::VALUE_OPTIONAL,"Application's name. Default to 'Default Application' if not specified.",'Default Application');

        $this->setDescription('Create new selifa application in this installation.');
        $this->addUsage('selifa-create-app --key="admin"');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $appCount = $this->Core->ComposeData->GetApplicationCount();
        $output->writeln('No. of application: <fg=green>'.$appCount.'</>',true);

        $fileGen = new FileGenerator();
        if ($appCount <= 0)
        {
            $output->write('Generating initialization file ',false);
            $initFile = ($this->Core->RootDir.'initialization.php');
            $fileGen->GenerateInitializationFile($initFile);
            $output->write('[ <fg=green>OK</> ]',true);
        }

        $appKey = $input->getOptions()['key'];
        $appName = $input->getOptions()['name'];

        $apm = new Application($this->Core,$fileGen);
        $isSuccess = $apm->Create($appKey,$appName);

        if ($isSuccess)
            $output->write('Application <fg=green>'.$appKey.'</> created.',true);
        else
            $output->write('<fg=red>Could not create application.</>',true);
    }
}
?>