<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;
use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginInterface;

/**
 * Class SelifaPlugin
 * @package RBS\Selifa\Composer
 */
class SelifaPlugin implements PluginInterface, Capable, EventSubscriberInterface
{
    /**
     * @var null|Core
     */
    public $CoreObject = null;

    /**
     * @var null|IOInterface
     */
    public $IO = null;

    /**
     * @param Composer $composer
     * @param IOInterface $io
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        Session::Initialize();

        $this->IO = $io;
        $this->CoreObject = Core::Initialize($composer,$io);
        $installer = new SelifaLibraryInstaller($this->CoreObject);
        $composer->getInstallationManager()->addInstaller($installer);
    }

    public function __destruct()
    {
        $this->CoreObject->Finalize();
    }

    /**
     * @return array
     */
    public function getCapabilities()
    {
        return array(
            'Composer\Plugin\Capability\CommandProvider' => 'RBS\Selifa\Composer\CommandProvider'
        );
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            'pre-install-cmd' => 'onPreInstallUpdateEvent',
            'pre-update-cmd' => 'onPreInstallUpdateEvent',
            'post-install-cmd' => 'onPostInstallUpdateEvent',
            'post-update-cmd' => 'onPostInstallUpdateEvent'
        );
    }

    /**
     *
     */
    public function onPreInstallUpdateEvent()
    {
        $this->IO->write('Selifa Composer Plugin: <fg=cyan>installed</>');
        Session::ResetCounts();
    }

    /**
     *
     */
    public function onPostInstallUpdateEvent()
    {
        $iCount = Session::GetCount('ConfigurationInstall');
        $aCount = $this->CoreObject->ComposeData->GetApplicationCount();
        if (($iCount > 0) && ($aCount > 0))
        {
            //update application due to configuration installs.
            $this->IO->write('<fg=green>'.$iCount.'</> configuration(s) are installed and <fg=green>'.$aCount.'</> application(s) installed. Updating application(s).',true);

            $apm = new Application($this->CoreObject,$this->CoreObject->Generator);
            $apps = $this->CoreObject->ComposeData->GetApplicationKeys();
            foreach ($apps as $appKey)
            {
                $apm->UpdateConfigurationOnly($appKey);
            }
        }
        $this->CoreObject->CreateGitIgnoreFile();
    }
}
?>